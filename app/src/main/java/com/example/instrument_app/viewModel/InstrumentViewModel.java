package com.example.instrument_app.viewModel;

import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.media.MediaRecorder;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.instrument_app.activity.AddInstrumentActivity;
import com.example.instrument_app.model.Instrument;
import com.example.instrument_app.repositories.InstrumentRepository;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class InstrumentViewModel extends AndroidViewModel {
    private InstrumentRepository repository;
    private LiveData<List<Instrument>> allInstruments;
    private LiveData<List<Instrument>> categoryInstrumentsStrun;
    private LiveData<List<Instrument>> categoryInstrumentsBici;
    private LiveData<List<Instrument>> categoryInstrumentsDech;

    public InstrumentViewModel(@NonNull Application application) {
        super(application);
        repository = new InstrumentRepository(application);
        allInstruments = repository.getAllInstruments();
        categoryInstrumentsStrun = repository.getInstrumentsStrun();
        categoryInstrumentsBici = repository.getInstrumentsBici();
        categoryInstrumentsDech = repository.getInstrumentsDech();
    }

    public void insert(Instrument instrument, Bitmap image) {

        if (image != null) {
            Date date = Calendar.getInstance().getTime();
            String formatedDate = new SimpleDateFormat("ddMMyyhhmmss", Locale.getDefault()).format(date);
            String imageName = "IMG" + formatedDate + ".jpg";
            String imagePath = saveImage(image, imageName);
            instrument.imagePath = imagePath;
        }

        repository.insert(instrument);
    }

    public void delete(Instrument instrument) {
        repository.delete(instrument);
    }

    private String saveImage(Bitmap image, String imageName) {
        ContextWrapper contextWrapper = new ContextWrapper(getApplication().getApplicationContext());
        File directory = contextWrapper.getDir("images", Context.MODE_PRIVATE);
        File path = new File(directory, imageName);

        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(path);
            image.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fileOutputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath() + "/" + imageName;
    }

    public LiveData<List<Instrument>> getAllInstruments() {
        return allInstruments;
    }

    public LiveData<List<Instrument>> getInstrumentsStrun() {
        return categoryInstrumentsStrun;
    }

    public LiveData<List<Instrument>> getCategoryInstrumentsBici() {
        return categoryInstrumentsBici;
    }

    public LiveData<List<Instrument>> getCategoryInstrumentsDech() {
        return categoryInstrumentsDech;
    }
}
