package com.example.instrument_app.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.instrument_app.model.Instrument;

import java.util.List;

@Dao
public interface InstrumentDao {
    @Insert
    void insert(Instrument instrument);

    @Delete
    void delete(Instrument instrument);

    @Query("SELECT * FROM instruments order by instrument_name asc")
    LiveData<List<Instrument>> getAllInstruments();

    @Query("select * from instruments i join  categories c on i.category_id = c.category_id where c.category_id = :id")
    LiveData<List<Instrument>> getInstrumentsByCategory(int id);
}
