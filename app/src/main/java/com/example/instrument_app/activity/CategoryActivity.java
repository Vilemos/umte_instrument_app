package com.example.instrument_app.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.instrument_app.R;
import com.example.instrument_app.adapters.CategoryAdapter;
import com.example.instrument_app.model.Category;
import com.example.instrument_app.viewModel.CategoryViewModel;

import java.util.List;

public class CategoryActivity extends AppCompatActivity {
    private CategoryViewModel categoryViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        CategoryAdapter adapter = new CategoryAdapter();
        recyclerView.setAdapter(adapter);

        categoryViewModel = new ViewModelProvider(
                this, ViewModelProvider.AndroidViewModelFactory.getInstance(this.getApplication()))
                .get(CategoryViewModel.class);
        categoryViewModel.getAllCategories().observe(this, adapter::setCategories);

        adapter.setOnItemClickListener(category -> {
            Intent intent = new Intent(CategoryActivity.this, CategoryDetailActivity.class);
            intent.putExtra("category_id", category);
            startActivity(intent);
        });
    }
}