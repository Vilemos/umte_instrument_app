package com.example.instrument_app.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.instrument_app.R;
import com.example.instrument_app.model.Instrument;
import com.example.instrument_app.viewModel.InstrumentViewModel;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

public class AddInstrumentActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private FusedLocationProviderClient fusedLocationProviderClient;
    InstrumentViewModel instrumentViewModel;
    private final int ACCESS_COARSE_LOCATION_REQUEST_CODE = 0;
    private final int RECORD_AUDIO_REQUEST_CODE = 1;
    static final int REQUEST_IMAGE_CAPTURE = 1;

    private double latitude;
    private double longitude;
    int categoryValue;
    private Bitmap instrumentImage;
    private ImageView imageView;

    private EditText editTextName;
    private EditText editTextOwner;
    private Spinner categoryPicker;

    MediaRecorder recording;
    String recordingName;
    MediaPlayer mediaPlayer;
    File path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_instrument);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        setTitle("Přidat nástroj");

        instrumentViewModel = new ViewModelProvider(this).get(InstrumentViewModel.class);
        editTextName = findViewById(R.id.edit_text_name);
        editTextOwner = findViewById(R.id.edit_text_owner);
        categoryPicker = findViewById(R.id.category_picker);
        imageView = findViewById(R.id.image_view);

        Button btnGetLocation = findViewById(R.id.btnGetLocation);
        Button btnStartRec = findViewById(R.id.btnStartRec);
        Button btnStopRec = findViewById(R.id.btnStopRec);
        btnStopRec.setEnabled(false);
        Button btnPlay = findViewById(R.id.btnPlay);
        btnPlay.setEnabled(false);
        Button btnStop = findViewById(R.id.btnStop);
        btnStop.setEnabled(false);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.categories, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categoryPicker.setAdapter(adapter);
        categoryPicker.setOnItemSelectedListener(this);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        btnGetLocation.setOnClickListener(v -> {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                getLocation();
            } else {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, ACCESS_COARSE_LOCATION_REQUEST_CODE);
            }
        });

        btnStartRec.setOnClickListener(v -> {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {

                ContextWrapper contextWrapper = new ContextWrapper(getApplication().getApplicationContext());
                File directory = contextWrapper.getDir("recordings", Context.MODE_PRIVATE);
                recordingName = UUID.randomUUID().toString() + "record.3gp";
                path = new File(directory, recordingName);

                setupMediaRecorder();
                try {
                    recording.prepare();
                    recording.start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                btnPlay.setEnabled(false);
                btnStop.setEnabled(false);
                btnStopRec.setEnabled(true);
                Toast.makeText(this, "Nahrávání...", Toast.LENGTH_SHORT).show();
            } else {
                requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, RECORD_AUDIO_REQUEST_CODE);
            }
        });

        btnStopRec.setOnClickListener(v -> {
            recording.stop();
            btnStopRec.setEnabled(false);
            btnPlay.setEnabled(true);
            btnStartRec.setEnabled(true);
            btnStop.setEnabled(false);
        });

        btnPlay.setOnClickListener(v -> {
            btnPlay.setEnabled(false);
            btnStop.setEnabled(true);
            btnStopRec.setEnabled(false);
            btnStartRec.setEnabled(false);

            mediaPlayer = new MediaPlayer();
            try {
                mediaPlayer.setDataSource(path.toString());
                mediaPlayer.prepare();

            } catch (IOException e) {
                e.printStackTrace();
            }
            mediaPlayer.start();
            Toast.makeText(this, "Hraje...", Toast.LENGTH_SHORT).show();
        });

        btnStop.setOnClickListener(v -> {
            btnStopRec.setEnabled(false);
            btnStartRec.setEnabled(false);
            btnStop.setEnabled(false);
            btnPlay.setEnabled(true);

            if (mediaPlayer != null) {
                mediaPlayer.stop();
                mediaPlayer.release();
            }
        });
    }

    @SuppressLint("NewApi")
    private void setupMediaRecorder() {
        recording = new MediaRecorder();
        recording.setAudioSource(MediaRecorder.AudioSource.MIC);
        recording.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recording.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        recording.setOutputFile(path);
    }

    private void saveInstrument() {
        Instrument instrument = new Instrument();
        instrument.instrumentName = editTextName.getText().toString();
        instrument.ownerName = editTextOwner.getText().toString();
        instrument.categoryId = categoryValue;
        instrument.latitude = String.valueOf(latitude);
        instrument.longitude = String.valueOf(longitude);
        if (path != null) {
            instrument.recordingPath = path.toString();
        }

        if (instrument.instrumentName.trim().isEmpty() || instrument.ownerName.trim().isEmpty()) {
            Toast.makeText(this, "Prosím vyplňte název a majitele", Toast.LENGTH_SHORT).show();
            return;
        }

        instrumentViewModel.insert(instrument, instrumentImage);
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.add_instrument_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_instrument:
                saveInstrument();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void createPhoto(View view) {
        Intent photographIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            startActivityForResult(photographIntent, REQUEST_IMAGE_CAPTURE);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "Nastala chyba s otevřením fotoaparátu", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("MissingPermission")
    private void getLocation() {
        fusedLocationProviderClient.getLastLocation()
                .addOnSuccessListener(this, location -> {
                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                    } else {
                        location = new Location("");
                        latitude = 50.1891;
                        longitude = 15.8224;
                        location.setLatitude(latitude);
                        location.setLongitude(longitude);
                    }

                    Log.e("location", +location.getLatitude() + " , " + location.getLongitude());
                })
                .addOnFailureListener(this, e -> Log.e("location", "location not ok" + e.getMessage()));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ACCESS_COARSE_LOCATION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocation();
                }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getItemAtPosition(position).toString()) {
            case "Strunné nástroje":
                categoryValue = 1;
                break;
            case "Bicí nástroje":
                categoryValue = 2;
                break;
            case "Dechové nástroje":
                categoryValue = 3;
                break;
            default:
                categoryValue = 1;
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap image = (Bitmap) extras.get("data");
            instrumentImage = image;
            imageView.setImageBitmap(image);
        }
    }
}