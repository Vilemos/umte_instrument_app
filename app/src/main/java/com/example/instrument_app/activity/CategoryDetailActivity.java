package com.example.instrument_app.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Switch;
import android.widget.Toast;

import com.example.instrument_app.R;
import com.example.instrument_app.adapters.InstrumentAdapter;
import com.example.instrument_app.model.Instrument;
import com.example.instrument_app.viewModel.InstrumentViewModel;

import java.util.List;

public class CategoryDetailActivity extends AppCompatActivity {
    private InstrumentViewModel instrumentViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        InstrumentAdapter adapter = new InstrumentAdapter();
        recyclerView.setAdapter(adapter);

        instrumentViewModel = new ViewModelProvider(
                this, ViewModelProvider.AndroidViewModelFactory.getInstance(this.getApplication()))
                .get(InstrumentViewModel.class);
        Intent intent = getIntent();
        if (intent.hasExtra("category_id")) {
            int id = intent.getIntExtra("category_id", 0);
            switch (id) {
                case 1:
                    instrumentViewModel.getInstrumentsStrun().observe(this, adapter::setInstruments);
                    break;
                case 2:
                    instrumentViewModel.getCategoryInstrumentsBici().observe(this, adapter::setInstruments);
                    break;
                case 3:
                    instrumentViewModel.getCategoryInstrumentsDech().observe(this, adapter::setInstruments);
                    break;
                default:
                    Toast.makeText(this, "Žádné položky", Toast.LENGTH_SHORT).show();
                    break;
            }
        } else {
            Toast.makeText(this, "Žádné položky", Toast.LENGTH_SHORT).show();
        }

        adapter.setOnItemClickListener(instrument -> {
            Intent intent1 = new Intent(CategoryDetailActivity.this, DetailActivity.class);
            intent1.putExtra("instrument_id", instrument);
            startActivity(intent1);
        });
    }
}