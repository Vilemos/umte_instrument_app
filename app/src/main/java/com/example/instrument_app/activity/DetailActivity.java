package com.example.instrument_app.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.instrument_app.R;
import com.example.instrument_app.model.Instrument;
import com.example.instrument_app.viewModel.InstrumentViewModel;

import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;

public class DetailActivity extends AppCompatActivity {
    InstrumentViewModel instrumentViewModel;
    Instrument instrument;
    MediaPlayer mediaPlayer;
    String category;

    private TextView textViewInstrumentName;
    private TextView textViewCategory;
    private TextView textViewOwnerName;
    private TextView textViewLatitude;
    private TextView textViewLongitude;
    private ImageView imageViewDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        if (intent.hasExtra("instrument_id")) {
            instrument = (Instrument) intent.getSerializableExtra("instrument_id");
            instrumentViewModel = new ViewModelProvider(
                    this, ViewModelProvider.AndroidViewModelFactory.getInstance(this.getApplication()))
                    .get(InstrumentViewModel.class);
        }

        switch (instrument.categoryId) {
            case 1:
                category = "Strunné nástroje";
                break;
            case 2:
                category = "Dechové nástroje";
                break;
            case 3:
                category = "Bicí nástroje";
                break;
        }

        textViewInstrumentName = findViewById(R.id.text_view_instrument_name);
        textViewCategory = findViewById(R.id.category);
        textViewOwnerName = findViewById(R.id.text_view_owner_name);
        textViewLatitude = findViewById(R.id.latitude);
        textViewLongitude = findViewById(R.id.longitude);
        imageViewDetail = findViewById(R.id.image_view_detail);
        Button play = findViewById(R.id.btnPlay);
        Button stop = findViewById(R.id.btnStop);
        play.setVisibility(View.INVISIBLE);
        stop.setVisibility(View.INVISIBLE);
        stop.setEnabled(false);

        textViewInstrumentName.setText(instrument.instrumentName);
        textViewCategory.setText(category);
        textViewOwnerName.setText(instrument.ownerName);
        textViewLatitude.setText(instrument.latitude);
        textViewLongitude.setText(instrument.longitude);
        if (instrument.imagePath != null && !instrument.imagePath.isEmpty()) {
            try {
                File file = new File(instrument.imagePath);
                if (file.exists()) {
                    imageViewDetail.setImageURI(Uri.fromFile(file));
                }
            } catch (Exception e) {
                Toast.makeText(this, "Nepodařilo se načíst obrázek", Toast.LENGTH_SHORT).show();
            }
        }
        if (instrument.recordingPath != null && !instrument.recordingPath.isEmpty()) {
            try {
                File file = new File(instrument.recordingPath);
                if (file.exists()) {
                    play.setVisibility(View.VISIBLE);
                    stop.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        play.setOnClickListener(v -> {
            stop.setEnabled(true);
            play.setEnabled(false);
            mediaPlayer = new MediaPlayer();

            try {
                mediaPlayer.setDataSource(instrument.recordingPath);
                mediaPlayer.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            }
            mediaPlayer.start();
            Toast.makeText(this, "Hraje...", Toast.LENGTH_SHORT).show();
        });

        stop.setOnClickListener(v -> {
            stop.setEnabled(false);
            play.setEnabled(true);

            if (mediaPlayer != null) {
                mediaPlayer.stop();
                mediaPlayer.release();
            }
        });
    }
}