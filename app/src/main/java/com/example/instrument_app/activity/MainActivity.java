package com.example.instrument_app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.instrument_app.R;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private Button categoryButton;
    private Button instrumentButton;
    private Button addInstrumentButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        categoryButton = findViewById(R.id.category_button);
        categoryButton.setOnClickListener(v -> openCategoryActivity());

        instrumentButton = findViewById(R.id.instrument_button);
        instrumentButton.setOnClickListener(v -> openInstrumentActivity());

        addInstrumentButton = findViewById(R.id.add_instrument_button);
        addInstrumentButton.setOnClickListener(v -> openAddInstrumentActivity());
    }

    public void openCategoryActivity() {
        Intent intent = new Intent(this, CategoryActivity.class);
        startActivity(intent);
    }

    public void openInstrumentActivity() {
        Intent intent = new Intent(this, InstrumentActivity.class);
        startActivity(intent);
    }

    public void openAddInstrumentActivity() {
        Intent intent = new Intent(this, AddInstrumentActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}