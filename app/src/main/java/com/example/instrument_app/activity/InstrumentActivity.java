package com.example.instrument_app.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.instrument_app.R;
import com.example.instrument_app.adapters.CategoryAdapter;
import com.example.instrument_app.adapters.InstrumentAdapter;
import com.example.instrument_app.model.Category;
import com.example.instrument_app.model.Instrument;
import com.example.instrument_app.viewModel.InstrumentViewModel;

import java.util.List;

public class InstrumentActivity extends AppCompatActivity {
    private InstrumentViewModel instrumentViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instrument);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        InstrumentAdapter adapter = new InstrumentAdapter();
        recyclerView.setAdapter(adapter);

        instrumentViewModel = new ViewModelProvider(
                this, ViewModelProvider.AndroidViewModelFactory.getInstance(this.getApplication()))
                .get(InstrumentViewModel.class);
        instrumentViewModel.getAllInstruments().observe(this, adapter::setInstruments);

        adapter.setOnItemClickListener(instrument -> {
            Intent intent = new Intent(InstrumentActivity.this, DetailActivity.class);
            intent.putExtra("instrument_id", instrument);
            startActivity(intent);
        });

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                instrumentViewModel.delete(adapter.getInstrumentAt(viewHolder.getAdapterPosition()));
                Toast.makeText(InstrumentActivity.this, "Nástroj vymazán", Toast.LENGTH_SHORT).show();
            }
        }).attachToRecyclerView(recyclerView);
    }
}