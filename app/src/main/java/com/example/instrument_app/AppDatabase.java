package com.example.instrument_app;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.instrument_app.dao.CategoryDao;
import com.example.instrument_app.dao.InstrumentDao;
import com.example.instrument_app.model.Category;
import com.example.instrument_app.model.Instrument;

@Database(entities = {Instrument.class, Category.class}, version = 2, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase instance;

    public static synchronized AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    AppDatabase.class, "instrument_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }
        return instance;
    }

    public abstract InstrumentDao instrumentDao();

    public abstract CategoryDao categoryDao();

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {
        private InstrumentDao instrumentDao;
        private CategoryDao categoryDao;

        private PopulateDbAsyncTask(AppDatabase db) {
            instrumentDao = db.instrumentDao();
            categoryDao = db.categoryDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            categoryDao.insert(new Category("Strunné nástroje"));
            categoryDao.insert(new Category("Bicí nástroje"));
            categoryDao.insert(new Category("Dechové nástroje"));
            return null;
        }
    }
}
