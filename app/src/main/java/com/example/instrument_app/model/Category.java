package com.example.instrument_app.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "categories")
public class Category {

    public Category(String categoryName) {
        this.categoryName = categoryName;
    }

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "category_id")
    public int id;

    @ColumnInfo(name = "category_name")
    public String categoryName;
}
