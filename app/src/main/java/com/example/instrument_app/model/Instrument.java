package com.example.instrument_app.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "instruments",
        foreignKeys = {
                @ForeignKey(entity = Category.class, parentColumns = "category_id", childColumns = "category_id")
        })
public class Instrument implements Serializable {

    public Instrument(String instrumentName, String latitude, String longitude, int categoryId, String ownerName, String imagePath, String recordingPath) {
        this.instrumentName = instrumentName;
        this.latitude = latitude;
        this.longitude = longitude;
        this.categoryId = categoryId;
        this.ownerName = ownerName;
        this.imagePath = imagePath;
        this.recordingPath = recordingPath;
    }

    public Instrument() {
    }

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "instrument_id")
    public int id;

    @ColumnInfo(name = "instrument_name")
    public String instrumentName;

    @ColumnInfo(name = "latitude")
    public String latitude;

    @ColumnInfo(name = "longitude")
    public String longitude;

    @ColumnInfo(name = "category_id", index = true)
    public int categoryId;

    @ColumnInfo(name = "owner_name")
    public String ownerName;

    @ColumnInfo(name = "image_path")
    public String imagePath;

    @ColumnInfo(name = "recording_path")
    public String recordingPath;
}
