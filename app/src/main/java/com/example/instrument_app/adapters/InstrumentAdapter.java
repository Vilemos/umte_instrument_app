package com.example.instrument_app.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.instrument_app.R;
import com.example.instrument_app.model.Instrument;

import java.util.ArrayList;
import java.util.List;

public class InstrumentAdapter extends RecyclerView.Adapter<InstrumentAdapter.InstrumentHolder> {
    List<Instrument> instruments = new ArrayList<>();
    Instrument instrument = new Instrument();
    private InstrumentAdapter.OnItemClickListener listener;

    @NonNull
    @Override
    public InstrumentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.instrument_card, parent, false);
        return new InstrumentHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull InstrumentHolder holder, int position) {
        Instrument currentInstrument = instruments.get(position);
        holder.textViewInstrument.setText(currentInstrument.instrumentName);
        holder.textViewOwner.setText(currentInstrument.ownerName);
    }

    @Override
    public int getItemCount() {
        return instruments.size();
    }

    public void setInstruments(List<Instrument> instruments) {
        this.instruments = instruments;
        notifyDataSetChanged();
    }

    public void setInstrument(Instrument instrument) {
        this.instrument = instrument;
        notifyDataSetChanged();
    }

    public Instrument getInstrumentAt(int position) {
        return instruments.get(position);
    }

    class InstrumentHolder extends RecyclerView.ViewHolder {
        private TextView textViewInstrument;
        private TextView textViewOwner;

        public InstrumentHolder(@NonNull View itemView) {
            super(itemView);
            textViewInstrument = itemView.findViewById(R.id.text_view_instrument);
            textViewOwner = itemView.findViewById(R.id.text_view_owner);
            itemView.setOnClickListener(v -> {
                int position = getAdapterPosition();
                if (listener != null && position != RecyclerView.NO_POSITION) {
                    listener.onItemClick(instruments.get(position));
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Instrument instrument);
    }

    public void setOnItemClickListener(InstrumentAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }
}
