package com.example.instrument_app.repositories;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.instrument_app.AppDatabase;
import com.example.instrument_app.dao.InstrumentDao;
import com.example.instrument_app.model.Instrument;

import java.util.List;

public class InstrumentRepository {
    private InstrumentDao instrumentDao;
    private LiveData<List<Instrument>> allInstruments;
    private LiveData<List<Instrument>> categoryInstrumentsStrun;
    private LiveData<List<Instrument>> categoryInstrumentsBici;
    private LiveData<List<Instrument>> categoryInstrumentsDech;

    public InstrumentRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        instrumentDao = database.instrumentDao();
        allInstruments = instrumentDao.getAllInstruments();
        categoryInstrumentsStrun = instrumentDao.getInstrumentsByCategory(1);
        categoryInstrumentsBici = instrumentDao.getInstrumentsByCategory(2);
        categoryInstrumentsDech = instrumentDao.getInstrumentsByCategory(3);
    }

    public void insert(Instrument instrument) {
        new InsertInstrumentAsyncTask(instrumentDao).execute(instrument);
    }

    public void delete (Instrument instrument){
        new DeleteInstrumentAsyncTask(instrumentDao).execute(instrument);
    }

    public LiveData<List<Instrument>> getAllInstruments() {
        return allInstruments;
    }

    public LiveData<List<Instrument>> getInstrumentsStrun() {
        return categoryInstrumentsStrun;
    }

    public LiveData<List<Instrument>> getInstrumentsBici() {
        return categoryInstrumentsBici;
    }

    public LiveData<List<Instrument>> getInstrumentsDech() {
        return categoryInstrumentsDech;
    }

    private static class InsertInstrumentAsyncTask extends AsyncTask<Instrument, Void, Void> {
        private InstrumentDao instrumentDao;

        private InsertInstrumentAsyncTask(InstrumentDao instrumentDao) {
            this.instrumentDao = instrumentDao;
        }

        @Override
        protected Void doInBackground(Instrument... instruments) {
            instrumentDao.insert(instruments[0]);
            return null;
        }
    }

    private static class DeleteInstrumentAsyncTask extends AsyncTask<Instrument, Void, Void> {
        private InstrumentDao instrumentDao;

        private DeleteInstrumentAsyncTask(InstrumentDao instrumentDao) {
            this.instrumentDao = instrumentDao;
        }

        @Override
        protected Void doInBackground(Instrument... instruments) {
            instrumentDao.delete(instruments[0]);
            return null;
        }
    }
}
